        //Send to Public Health
        OnClickListener onHL7ClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Record needs to have a health card number for this
                if(TextUtils.isEmpty(record.getHealthCardID())){
                    new AlertDialog(getActivity(), "Error", "Record needs to have a healthcard number for this action")
                            .show();
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //Set up the server socket
                            Socket serverSocket = new Socket("142.157.72.251", 6661);

                            //Set up the client socket
                            Socket clientSocket = new Socket();
                            //Connect the client socket to the server socket
                            clientSocket.connect(serverSocket.getRemoteSocketAddress());

                            //Start the message
                            String message = "";

                            //All of the fields below are followed by the separator character |
                            //Part 1: Message Header Segment

                            //Header
                            message += "MSH|";

                            //Encoding Characters
                            message += "^~\\&|";

                            //Sending Application
                            message += "ImmunizeCanada|";

                            //Sending Facility
                            message += "Sigvaria|";

                            //Receiving Application
                            message += "MirthConnect|";

                            //Receiving Facility
                            message += "Cameron|";

                            //Date/Time of Message
                            DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("yyyyMMddhhmmss");
                            message += timeFormatter.print(DateTime.now()) + "|";

                            //1 Space
                            message += "|";

                            //MessageType
                            message += "VXU^V04|";

                            //Message Control ID
                            //Unique so we take the time with the app name
                            String controlID = "ImmunizeCanada" + timeFormatter.print(DateTime.now());
                            message += controlID + "|";

                            //Processing ID
                            //P = Production Processing
                            message += "P|";

                            //Version ID of HL7
                            message += "2.5.1|";

                            //2 Spaces
                            message += "||";

                            //Accept Acknowledgement Type
                            message += "NE|";

                            //Wrap up the Message Header Segment
                            message += "\n";

                            //Part 2 : Patient Identification Segment
                            //Header
                            message += "PID|";

                            //2 Spaces
                            message += "||";

                            //Patient Identifier List (Health Card Number)
                            message += record.getHealthCardID() + "|";

                            //1 Space
                            message += "|";

                            //Patient Name
                            message += record.getLastName() + "^" + record.getFirstName() + "|";

                            //Mother's Maiden Name
                            //We are not using this field currently
                            message += "|";

                            //Date/Time of Birth
                            DateTimeFormatter birthFormatter = DateTimeFormat.forPattern("yyyyMMdd");
                            message += birthFormatter.print(record.getBirthDate()) + "|";

                            //Sex
                            if(record.getGender() == Gender.MALE){
                                message += "M|";
                            }
                            else{
                                message += "F|";
                            }

                            //1 Space
                            message += "|";

                            //Race
                            //We are not using this field currently
                            message += "|";

                            //Patient Address
                            //We are not using this field currently
                            message += "|";

                            //1 Space
                            message += "|";

                            //Phone Number
                            //We are not using this field currently
                            message += "|";

                            //8 Spaces
                            message += "||||||||";

                            //Ethnic Group
                            //We are not using this field currently
                            message += "|";

                            //1 Space
                            message += "|";

                            //Multiple Birth Indicator
                            //We are not using this field currently
                            message += "|";

                            //Close the PID Segment
                            message += "\n";

                            //Part 3: Order Request Segment
                            //Header
                             message += "ORC|";

                            //Order Control
                            //Always RE for VXU
                            message += "RE|";

                            //Close the ORC Segment
                            message += "\n";

                            //Go through the list of received doses
                            for(Dose dose : record.getDoses()){
                                //Only keep the received ones
                                if(!dose.isReceived()){
                                    continue;
                                }

                                //RXA : Pharmacy/Treatment Administration Segment Definition
                                //Header
                                message += "RXA|";

                                //Give Sub-ID Counter : Always 0
                                message += "0|";

                                //Administration Sub-ID Counter : Always 999
                                message += "999|";

                                //Date/Time Start of Administration
                                //Use same formatter as birthdate
                                message += birthFormatter.print(dose.getDate()) + "|";

                                //Date/Time End of Administration
                                //Same as start of administration
                                message += birthFormatter.print(dose.getDate()) + "|";

                                //Administered code
                                message += dose.getVaccinationReference().getCode() + "|";

                                //Administered Amount: Always 999
                                message += "999|";

                                //2 Spaces
                                message += "||";

                                //Administration Notes
                                //We are not using this field currently
                                message += "|";

                                //Administrating Provider
                                //Use the doctor here
                                message += record.getDoctor() + "|";

                                //Administered location
                                //Use the province here
                                message += record.getProvince() + "|";

                                //3 Spaces
                                message += "|||";

                                //Substance Lot Number
                                //We are not using this field currently
                                message += "|";

                                //1 Space
                                message += "|";

                                //Substance Manufacturer Name
                                //We are not using this field currently
                                message += "|";

                                //Close this segment
                                message += "\n";
                            }

                            Log.e("Message", message);

                            //Set up the printwriter to write onto the output stream of the client socket
                            //Turn autoflush on
                            PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                            //Write the message onto the output stream
                            writer.println(message);

                            clientSocket.shutdownOutput();

                            //Set up the reader for the client socket
                            BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                            //Get the first segment back
                            String messageHeaderResponse = reader.readLine();
                            Log.e("Message Header Response", messageHeaderResponse);

                            //Get the second segment back
                            String messageAckResponse = reader.readLine();
                            Log.e("Message Ack Response", messageAckResponse);

                            //Make an array, separate the values with the |
                            String[] responseData = messageAckResponse.split("\\|");

                            final String toastMessage;

                            //Check that we are dealing with the right segment
                            if(responseData[0].equals("MSA")){
                                //Acknowledged Message
                                if(responseData[1].equals("AA")){
                                    toastMessage = "Sent";
                                }
                                //Error
                                else{
                                    toastMessage = responseData[3];
                                }
                            }
                            //Something is wrong
                            else{
                                toastMessage = responseData[3];
                            }

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //Success message for the user
                                    Toast.makeText(getActivity(), toastMessage, Toast.LENGTH_SHORT).show();
                                }
                            });

                            //Close the socket
                            clientSocket.close();
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }).start();
            }
        };
        addViews.addButtonField("Send to Public Health", getResources().getColor(R.color.black), R.drawable.drawable_whitedefault_bluepressed, onHL7ClickListener);