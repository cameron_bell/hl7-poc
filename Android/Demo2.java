 //Send to Public Health
        OnClickListener onHL7ClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Record needs to have a health card number for this
                if(TextUtils.isEmpty(record.getHealthCardID())){
                    new AlertDialog(getActivity(), "Error", "Record needs to have a healthcard number for this action")
                            .show();
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            TrustManager[] trustAllCerts = new TrustManager[]{
                                new X509TrustManager(){
                                    public java.security.cert.X509Certificate[] getAcceptedIssuers(){
                                        return new X509Certificate[]{};
                                    }

                                    @Override
                                    public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws java.security.cert.CertificateException{}

                                    @Override
                                    public void checkServerTrusted(X509Certificate[] arg0, String arg1)throws java.security.cert.CertificateException{}
                                }
                            };

                            SSLContext sc = SSLContext.getInstance("SSL");
                            sc.init(null, trustAllCerts, null);

                            SocketIO.setDefaultSSLSocketFactory(sc);
                            SocketIO socket = new SocketIO();
                            socket.connect("https://142.157.73.110:55555", new IOCallback() {
                                @Override
                                public void onMessage(JSONObject json, IOAcknowledge ack) {
                                    try {
                                        Log.e("HL7", "Server said:" + json.toString(2));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onMessage(String data, IOAcknowledge ack) {
                                    Log.e("HL7", "Server said: " + data);
                                }

                                @Override
                                public void onError(SocketIOException socketIOException) {
                                    Log.e("HL7", "an Error occurred");
                                    socketIOException.printStackTrace();
                                }

                                @Override
                                public void onDisconnect() {
                                    Log.e("HL7", "Connection terminated.");
                                }

                                @Override
                                public void onConnect() {
                                    Log.e("HL7", "Connection established");
                                }

                                @Override
                                public void on(String event, IOAcknowledge ack, Object... args) {
                                    Log.e("HL7", "Server triggered event '" + event + "'");
                                    //Visual feedback when the server responds with a success message
                                    if(event.equals("success")){
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(), "Sent", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }
                            });

                            //Set up the JSON with the right info
                            //Required stuff: name, health card number, doses
                            //Start the JSON String
                            String json = "{";
                            //Add the first name
                            json += "\"firstName\" : \"" + record.getFirstName() + "\",";
                            //Add the last name
                            json += "\"lastName\" : \"" + record.getLastName() + "\",";
                            //Add the birthdate
                            json += "\"birthdate\" : \"" + record.getBirthDate().toString() + "\",";
                            //Add the province
                            json += "\"province\" : \"" + ProvinceManager.getProvinceString(record.getProvince(), getResources()) + "\",";
                            //Add the gender (int)
                            json += "\"gender\" : \"" + Gender.getGenderString(record.getGender(), getResources()) + "\",";
                            //Add the health card number
                            json += "\"hcin\" : \"" + record.getHealthCardID() + "\",";
                            //Add the doctor's name
                            json += "\"doctor\" : \"" + record.getDoctor() + "\",";
                            //Add the doses
                            //Beginning of doses array
                            json += "\"doses\" : [";
                            //Go through the doses
                            int count = 0;
                            for(Dose dose : record.getDoses()){
                                //Only keep the received ones
                                if(dose.isReceived()){
                                    //Add a comment for every dose a23fter the first one
                                    if(count != 0){
                                        json += ",";
                                    }

                                    //Beginning of dose object
                                    json += "{";
                                    //Add the code
                                    json += "\"code\" : \"" + dose.getVaccinationReference().getCode() + "\",";
                                    //Add the name
                                    json += "\"name\" : \"" + dose.getVaccinationReference().getName() + "\",";
                                    //Add the date received
                                    json += "\"dateReceived\" : \"" + dose.getDate().toString() + "\",";
                                    //Add the comments
                                    String comments = dose.getComments() == null ? "" : dose.getComments();
                                    json += "\"comments\" : \"" + comments + "\"";
                                    //End of dose object
                                    json += "}";

                                    //Increment count
                                    count ++;
                                }
                            }
                            //End of doses array
                            json += "]";
                            //End the json string
                            json += "}";

                            Log.e("JSON String", json);
                            JSONObject jsonObject = new JSONObject(json);

                            socket.send(jsonObject);
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (KeyManagementException e) {
                            e.printStackTrace();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            Log.e("HL7", "JSON Exception");
                            e.printStackTrace();
                        }
                    }

                }).start();
            }
        };
        addViews.addButtonField("Send to Public Health", getResources().getColor(R.color.black), R.drawable.drawable_whitedefault_bluepressed, onHL7ClickListener);