var fs = require('fs');
var path = require('path');
var mongoose = require('mongoose');
var jade = require('jade');


// Mongodb schema
var Schema = mongoose.Schema;

var PersonSchema = new Schema({
  hcin       : String,
  firstName     : String,
  lastName: String,
  vxus      : [],
  birthdate: String,
  lastUpdate : Date,
  gender:String,
  province:String,
  doctor:String,
  doses:[{

    code:String,
    name:String,
    dateReceived:String,
    comments:String
  }
  
  ]
});
/*
var uristring =
process.env.MONGOLAB_URI ||
process.env.MONGOHQ_URL ||
'mongodb://localhost/hl7-poc';


mongoose.connect(uristring, function (err, res) {
  if (err) {
  console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
  console.log ('Succeeded connected to: ' + uristring);
  }
});
*/



mongoose.connect('mongodb://localhost/immunize');
mongoose.model('Person', PersonSchema);

var Person = mongoose.model('Person');


/* EXPRESS STUFF */
var options = {
  key: fs.readFileSync('cert/private-key.pem'),
  cert: fs.readFileSync('cert/certificate.pem')
} 

var express = require('express.io');
var app = express();

app.https(options).io();
//app.http().io();


app.io.route('connect', function(req) {
  console.log('got connect');
  var people = Person.find({}, function(err, docs){
    console.log('length: ' + docs.length);
    req.io.emit('update',docs);
    console.log("updating all");
  });
});

//not working
app.io.route('reconnect',function(){
  console.log('reconnected');
  var people = Person.find({}, function(err, docs){
    console.log('length: ' + docs.length);
    req.io.emit('update',docs);
    console.log("updating all");
  });
});

app.io.route('anything',function(req) {
  console.log('GOT SOMETHING');
});

app.io.route('message',function(req) {
  console.log('got a message');
  console.log('data: ' + req.data);
  console.log('body: ' + req.body);

  if (req.data.hcin) {

    Person.find({hcin: req.data.hcin} ,function(err,docs) {

      if(docs.length == 0) {
        console.log(err);

        console.log("create person");
      //create the new person
      var person = new Person(req.data);

      //save the new person to the db
      person.save(function(err){
        if(err){
          console.log(err); 
        }else {
          console.log('Saved ' + person.id);
          console.log('broadcast');
          req.io.emit('success');
          broadcastUpdate();
        }
        
      });

    }else{
      console.log(docs.length);
      if(docs.length != 1) {
        console.log('oops: '+ docs.length); 
      }

      else{
        Person.update({_id:docs[0].id},req.data,function(){
          console.log('updated');
          console.log('broadcast');
          req.io.emit('success');
          broadcastUpdate();
        });
      }
    }



  });
  };

  req.io.respond({
    success: 'Thank you for your message. Love Server'
  });
});

app.io.route('delete',function(req){
  console.log('DELETE');
  console.log(req.data);

  Person.remove({_id:req.data._id},function(err){
    if(err) {
      console(err)
    }else{
      console.log('Deleted');
      broadcastUpdate();
    }


  });
});

function broadcastUpdate(req) {
  var people = Person.find({}, function(err, docs){
    console.log('length: ' + docs.length);
    app.io.broadcast('update',docs);
    console.log("updating all");
  });
}

app.io.route('connection',function(req){
  console.log('connection');
});
// Configuration
app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

// Send the client html.
app.get('/', function(req, res) {
  res.sendfile(__dirname + '/public/index.html');
  console.log("root");
});


// Show all
app.get('/people', function(req, res){
  res.sendfile(__dirname + '/public/people.html');
});

app.get('/people/remove/:id', function(req, res){
  Person.findById(req.params.id, function (err, person) {
    person.remove(person);
    console.log('Deleted ' + person.id)
    //res.redirect('/people');
    if (!err) {
    }
  });
});


var port = Number(process.env.PORT || 55555);

app.listen(port,"192.168.2.19",function(){
  console.log('Listening on 55555');
});



